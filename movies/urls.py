from django.urls import path
from .views import movie_list, upload_movie, movie_detail

urlpatterns = [
    path("", movie_list, name="movie_list"),
    path("upload/", upload_movie, name="upload_movie"),
    path("<int:id>/", movie_detail, name="movie_detail"),
]
