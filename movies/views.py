from django.shortcuts import render, get_object_or_404, redirect
from .models import Movie
from movies.forms import UploadMovie

# Create your views here.
def movie_detail(request, id):
    movie = get_object_or_404(Movie, id=id)
    context = {
        "movie_object": movie
    }
    return render(request, "movies/movie_detail.html", context)

def movie_list(request):
    movies = Movie.objects.all()
    context = {
        "movies" : movies
    }
    return render(request, "movies/movie_list.html", context)

def upload_movie(request):
    if request.method == "POST":
        form = UploadMovie(request.POST)
        if form.is_valid():
            form.save()
            return redirect("movie_list")
    else:
        form = UploadMovie()
    context = {
        "form" : form
    }
    return render(request, "movies/upload_movie.html", context)
