from django.forms import ModelForm
from movies.models import Movie

class UploadMovie(ModelForm):
    class Meta:
        model = Movie
        fields = [
            "title",
            "image",
            "description",
            "release_date",
        ]
