from django.db import models

# Create your models here.
class Movie(models.Model):
    title = models.CharField(max_length=200)
    image = models.URLField()
    description = models.TextField()
    release_date = models.DateField()

    def __name__(self):
        print(self.name)
